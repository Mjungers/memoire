from sklearn.svm import SVC

from Machine_learning.Tools.metaparameters import optimize_parameters


def select_C_and_tol():
    """
    select the optimal value of the metaparameters C and tol
    :return: None
    """
    clf = SVC(kernel="linear", class_weight='balanced')
    parameters = {"C": [0.1,1,10,100,1000],"tol": [1e-2,1e-3,1e-4, 1e-5]}
    optimize_parameters(clf,parameters)




if __name__ == "__main__":
    select_C_and_tol()