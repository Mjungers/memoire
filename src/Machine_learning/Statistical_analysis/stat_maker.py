import re
import statistics

import pandas as pd
from pingouin import mwu

from Machine_learning.Dataset.dataset import load_dataset
from Machine_learning.Feature_engeniering.feature_info import NON_TEXT_PACK_FEATURES, NON_TEXT_PACK_FEATURE_NAME
from Machine_learning.Feature_engeniering.get_feature import get_text_features_names
from paths import CSV_FILE, STAT_OUTPUT, CSV_EVEN_FILE


def get_stats(feature_VCC,feature_other):
    """
    compute the mean and the man-whitney U test values for all the features
    :param feature_VCC: a matrix that contain all the values of the features of VCCommits
    :param feature_other: a matrix that contain all the values of the features of non VCCommits
    :return:
    """
    assert len(feature_VCC) == len(feature_other)
    nbr_feature = len(feature_VCC)
    stat_list= []
    mu_list = []
    effect_list = []
    rank_list=[]
    for feature_index in range(nbr_feature):
        try:
            stat_tmp = mwu(feature_VCC[feature_index],feature_other[feature_index],tail='one-sided')
            stat_list.append(stat_tmp.iloc[0, 0])
            mu_list.append(stat_tmp.iloc[0, 2])
            rank_list.append(stat_tmp.iloc[0, 3])
            effect_list.append(stat_tmp.iloc[0, 4])
            #effect_list.append(compute_effsize(feature_VCC[feature_index],feature_other[feature_index]))
        except ValueError as e:
            print(str(e))
            stat_list.append(None)
            mu_list.append(None)
            rank_list.append(None)
            effect_list.append(None)
    return stat_list,mu_list,rank_list,effect_list

def features_matrix(commit_matrix):
    """

    :param commit_matrix:
    :return:
    """
    nbr_feature = len(commit_matrix[0])
    features_matrix = []
    for feature_id in range(nbr_feature):
        tmp_feature_list = [commit_data[feature_id] for commit_data in commit_matrix]
        features_matrix.append(tmp_feature_list)
    return features_matrix


def effect_size(size_true,size_false,u_result):
    """
    computes the effect size of the spesific feature
    function deprecated
    :param size_true:
    :param size_false:
    :param u_result:
    :return:
    """
    if u_result != None:
        return round((u_result/(size_true*size_false))*100,2)
    else:
        return None

def get_stat_datframe(X,Y,collum_name):
    """
    computes the stat for all the features in the dataframe
    :param X: The value features
    :param Y: the label of the value
    :param collum_name: the name of features
    :return: a panda dataframe that contains the result of the statistical analysis
    """
    X_true = []
    X_false = []
    for i in range(len(X)):
        if Y[i]:
            X_true.append(X[i])
        else:
            X_false.append(X[i])
    print(len(X_true))
    feature_true = features_matrix(X_true)
    features_false = features_matrix(X_false)
    mean_VCC = list(map(statistics.mean, feature_true))
    mean_other = list(map(statistics.mean, features_false))
    U_test, mu_test , rank_test, effect_test = get_stats(feature_true, features_false)
    #effect_test = list(map(effect_size, repeat(size_VCC), repeat(size_other), U_test))
    dataframe = pd.DataFrame([mean_VCC, mean_other, U_test, mu_test, effect_test,rank_test],
                             ["mean_VCC","mean_other","U_value","mu","effec_size","rank_test"], collum_name)

    return dataframe

def stat_to_tex():
    """
    print the result of the statistical analysis in tex code
    :return: None
    """

    X, Y, columns = load_dataset(CSV_FILE)
    r = re.compile(r"Comp_.*")
    comp_features = list(filter(r.match, list(columns)))
    NON_TEXT_PACK_FEATURES[-1] = NON_TEXT_PACK_FEATURES[-1]+ comp_features
    for (features_names, name) in zip(NON_TEXT_PACK_FEATURES, NON_TEXT_PACK_FEATURE_NAME):
        features_id = columns.get_indexer(features_names)
        print(name)

        X_temp = [item[features_id] for item in X]
        dataframe = get_stat_datframe(X_temp, Y, features_names).transpose()
        dataframe.pop("rank_test")
        print(dataframe.to_latex())
    X, Y, columns = load_dataset(CSV_EVEN_FILE)
    text_features = get_text_features_names(columns)
    for (features_names, name) in zip(text_features,["VCC_BOW_message","VCC_BOW_content","VCC_keywords","BOW_features","comment_features","keywords"]):
        features_id = columns.get_indexer(features_names)
        print(name)
        print(features_id)
        X_temp = [item[features_id] for item in X]
        dataframe = get_stat_datframe(X_temp, Y, features_names).transpose()
        dataframe.pop("rank_test")
        print(dataframe.sort_values(by=['mu']).head(10).to_latex())



def compute_stats():
    """
    computes the stats and saves them to a csv file
    :return:
    """
    split = 41
    df = pd.read_csv(CSV_FILE, header=0)
    X = df.iloc[:, 1:split].values
    X2 = df.iloc[:, split:].values
    Y = df.iloc[:, 0].values
    dataframe = get_stat_datframe(X,Y,df.columns[1:split].values)
    dataframe = dataframe.sort_values(by='effect-size', axis=1, ascending=[False])
    dataframe.to_csv(STAT_OUTPUT/'result.csv',na_rep=0,header=True)
    """
    dataframe_commit = get_stat_datframe(X2,Y,df.columns[split:].values)
    dataframe_commit.to_csv(STAT_OUTPUT/'result_commit.csv', na_rep=0, header=True)
    df_text = pd.read_csv(CSV_TEXT_DATASET_FILE, header=0)
    X_text = df_text.iloc[:,1:].values
    Y_text = df_text.iloc[:,0].values
    dataframe_text = get_stat_datframe(X_text, Y_text, df_text.columns[1:].values)
    dataframe_text.to_csv(STAT_OUTPUT/'result_text.csv',na_rep=0, header=True)"""


if __name__ == "__main__":
    stat_to_tex()

