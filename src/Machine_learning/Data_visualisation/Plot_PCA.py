import os

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import decomposition

from Machine_learning.Dataset.dataset import load_dataset
from paths import PROJECT_ROOT


def plot_datasets():
    """
    plot the repositories datasets in a 3D plane
    :return: None
    """
    dirList = os.listdir(str(PROJECT_ROOT)+"/output/dataset")  # current directory

    for dir in dirList:
        #if it correspond to a dataset plot it
        if os.path.isdir(str(PROJECT_ROOT)+"/output/dataset/"+dir) == True and dir.isdigit():
            X,Y,columns=load_dataset(str(PROJECT_ROOT)+"/output/dataset/"+dir+"/dataset.csv")
            show_3D(X,Y)

def show_3D(X,y):
    """
    Plot the 3D representation of the dual class dataset
    :param X: the data features matrix
    :param y: the data label
    :return: None
    """

    fig = plt.figure(1, figsize=(8, 6))
    plt.clf()
    ax = Axes3D(fig, elev=48, azim=134)

    plt.cla()
    pca = decomposition.PCA(n_components=3)

    X = pca.fit_transform(X)

    ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y,
               cmap=plt.cm.Set1, edgecolor='k')
    ax.set_title("First three PCA directions")
    ax.set_xlabel("1st eigenvector")
    ax.w_xaxis.set_ticklabels([])
    ax.set_ylabel("2nd eigenvector")
    ax.w_yaxis.set_ticklabels([])
    ax.set_zlabel("3rd eigenvector")
    ax.w_zaxis.set_ticklabels([])
    plt.show()


if __name__ == "__main__":
    plot_datasets() #plot some specific datasets
    #plot the final dataset
    X,Y,columns =load_dataset(PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv')
    show_3D(X,Y)