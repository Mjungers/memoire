from statistics import mean

import pandas as pd
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_validate, KFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
from sklearn.svm import SVC

from Machine_learning.Dataset.dataset import load_dataset
from Machine_learning.Tools.compare_algo import compare_score
from paths import PROJECT_ROOT


def compare_kernel(X,Y):
    """
    compare linearSVC with svc with other kernel to find if one is maybe more suited to our study
    :param X: An array of the features
    :type X: Array
    :param Y: An array of the class coresponding with X
    :type Y: Array
    :return:
    """
    clasifiers = []
    clasifiers.append(("linearSVC",LinearSVC(loss='hinge')))
    for kernel_type in ['linear', 'poly', 'rbf', 'sigmoid']:
        clasifiers.append((kernel_type,SVC(kernel=kernel_type,class_weight='balanced')))
    scoring = ['accuracy', 'precision', 'recall', 'f1']
    result = compare_score(X,Y,clasifiers,scoring)
    print(result.to_latex())

def compare_loss(X,Y):
    """
    compare linearSVC with svc with other kernel to find if one is maybe more suited to our study

    :param X: An array of the features
    :type X: Array
    :param Y: An array of the class coresponding with X
    :type Y: Array
    :return: None
    """
    clasifiers = []
    clasifiers.append(("loss:hinge",LinearSVC(loss='hinge')))
    clasifiers.append(("loss:squared_hinge", LinearSVC(loss='squared_hinge')))
    scoring = ['accuracy', 'precision', 'recall', 'f1']
    result = compare_score(X,Y,clasifiers,scoring)
    print(result.to_latex())


def compare_algo(X,Y):
    """
    This function is used to compare the result of these machine learning algorithm: ['RandomForest','DecisionTree','AdaBoost','KNeighbors','LogisticRegresion','SVM']
    with these scores: 'accuracy','precision','recall','f1','matthews_corrcoef','roc_auc'

    :param X: An array of the features
    :type X: Array
    :param Y: An array of the class coresponding with X
    :type Y: Array

    :return : Panda dataframe of the result
    :rtype : dataframe
    """
    scores = {'accuracy': 'accuracy', 'precision': 'precision', 'recall': 'recall', 'f1': 'f1'}

    models = [RandomForestClassifier(),
              AdaBoostClassifier(), KNeighborsClassifier(),
              LogisticRegression(max_iter=1000), make_pipeline(StandardScaler(),LinearSVC())]
    kfold = KFold(n_splits=10, shuffle=True)
    stats = {}
    for (model, name) in zip(models,
                             ['RandomForest', 'AdaBoost', 'KNeighbors', 'LogisticRegresion', 'SVM']):
        scores_result = cross_validate(model, X, Y, cv=kfold, scoring=scores)
        stats_temp = {}
        stats_temp['fit_time'] = mean(list(scores_result['fit_time']))
        stats_temp['accuracy'] = mean(list(scores_result['test_accuracy']))
        stats_temp['precision'] = mean(list(scores_result['test_precision']))
        stats_temp['recall'] = mean(list(scores_result['test_recall']))
        stats_temp['f1'] = mean(list(scores_result['test_f1']))
        stats[name] = stats_temp
    return pd.DataFrame.from_dict(stats)



if __name__ == "__main__":
    X, Y, columns = load_dataset(PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv')
    print(compare_algo(X,Y).to_latex())
    print(compare_kernel(X,Y))
    print(compare_loss(X,Y))
