from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC, SVC

from Machine_learning.Dataset.dataset import load_dataset
from Machine_learning.Feature_engeniering.feature_info import *
from Machine_learning.Feature_engeniering.get_feature import get_VCC_features_id, get_features_id
from Machine_learning.Tools.compare_evolution import evolution_score, evolution_pre_rec_curve
from Machine_learning.Tools.compare_features import compare_score
from paths import CSV_EVEN_FILE, PROJECT_ROOT


def linearsvc_evolution(dataset_name=CSV_EVEN_FILE):
    """
    print the result of the evolution of the various scorer with the linearsvc algorithm
    :param dataset_name The address of the .csv file to load
    :return: None
    """
    X, Y, columns = load_dataset(dataset_name)
    X = StandardScaler().fit_transform(X)
    clf = LinearSVC(max_iter=1000000)
    result = evolution_score(X,Y,columns,clf, ['accuracy', 'precision', 'recall', 'f1'],list(get_features_id(columns)),PACK_FEATURE_NAME)
    print(result.to_latex())


def compare_VCC_score():
    """
    Print the result of the comparison of the classification score wiht our final features and the VCCfinder features
    :return: None
    """
    X, Y, columns = load_dataset()
    clf = make_pipeline(StandardScaler(), LinearSVC(max_iter=1000000))
    final_X,final_Y, columns_final =load_dataset(PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv')
    VCC_id = get_VCC_features_id(columns)
    VCC_X = [item[VCC_id] for item in X]
    result = compare_score([VCC_X,final_X], [Y,final_Y], ["VCC", "Final_result"], clf, ['accuracy', 'precision', 'recall', 'f1'])
    print(result.to_latex())
    
def compare_VCC_prerec_curve():
    """
    Plot and show the precison recall curve to compare the result of VCCFinder and our results.
    :return: None
    """
    X, Y, columns = load_dataset()
    clf = make_pipeline(StandardScaler(), LinearSVC())
    X2, Y2,columns2 = load_dataset(PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv')
    VCC_id = get_VCC_features_id(columns)
    X = [item[VCC_id] for item in X]
    evolution_pre_rec_curve([X,X2],[Y,Y2],clf,["VCC","Final_result"])

def final_score():
    """
    Print the latex table containong the final result of our ml algorithm
    :return:  None
    """
    X, Y, columns = load_dataset()
    clf = make_pipeline(StandardScaler(), SVC(kernel='linear',C=0.1,tol=0.01))
    final_X, final_Y, columns_final = load_dataset(PROJECT_ROOT / "output" / "dataset" / 'final_dataset.csv')
    VCC_id = get_VCC_features_id(columns)
    VCC_X = [item[VCC_id] for item in X]
    result = compare_score([VCC_X, final_X], [Y, final_Y], ["VCC", "Final_result"], clf,
                           ['accuracy', 'precision', 'recall', 'f1'])
    print(result.to_latex())

if __name__ == "__main__":

    compare_VCC_score() #compare the result after the feature optimization

    linearsvc_evolution(dataset_name=PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv')# show the evolution of the score

    compare_VCC_prerec_curve()#plot the precision recall curve

    final_score()#show the comparaison after the parameter optimization

