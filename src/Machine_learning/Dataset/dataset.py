import gc
import os
import random

import pandas as pd

from Machine_learning.Feature_engeniering.compute_features import make_ml_features, text_feature, \
    compute_vectorizer_features
from paths import PROJECT_ROOT, CSV_FILE, CSV_TEXT_FILE, CSV_TEXT_DATASET_FILE, CSV_EVEN_FILE
from src.database.database_info import PORT, PASSWORD, USERNAME, NEW_DB, DATABASE_NAME
from src.database.load_db import Database


def db_to_dataset(db_name =DATABASE_NAME):
    """
    Create the dataset feature.csv from the database, it contains oll the numerical features
    :param db_name the name of the database to load the data
    :return: None
    """
    new_db = Database(PORT, USERNAME, PASSWORD, db_name)
    if db_name == NEW_DB:
        rep_ids = new_db.get_all_rep_id(False)
    else:
        rep_ids = new_db.get_all_rep_id(True)
    new_db.close_db()
    features =[]
    for id in rep_ids:
        print(id)

        new_features =make_ml_features(id,db_name)
        if new_features != None:
            features.extend(new_features)



    df = pd.DataFrame.from_dict(features)
    df.to_csv(CSV_FILE,na_rep=0,index=False,header=True)

def db_to_datasets(db_name=DATABASE_NAME):
    """
    Create the datasets  for each repositories from the database
    :param db_name the name of the database to load the data
    :return: None
    """
    new_db = Database(PORT, USERNAME, PASSWORD, db_name)
    if db_name== NEW_DB:
        rep_ids = new_db.get_all_rep_id(False)
    else:
        rep_ids = new_db.get_all_rep_id(True)
    new_db.close_db()
    for id in rep_ids:
        new_features =make_ml_features(id,db_name)
        if new_features != None:
            df = pd.DataFrame.from_dict(new_features)
            df= compute_vectorizer_features(df)
            os.mkdir(PROJECT_ROOT/"output"/"dataset"/str(id[0]))
            df.to_csv(PROJECT_ROOT/"output"/"dataset"/str(id[0])/'dataset.csv',na_rep=0,index=False,header=True)

def text_to_dataset():
    """
    load the text_feature dataset and vectorize the texts in order to create the vectors for each commit
    :return: None
    """
    df = pd.read_csv(CSV_TEXT_FILE,header=0)
    list_text = []
    features = []
    for step in range(0,df.shape[0],1000):
        for i in df.iloc[step:step+1000, 1:].values.astype('U'):
            list_text.append(i[0])
            list_text.append(i[1])
        list_status = df.iloc[step:step+1000,0].values
        features.extend(text_feature(list_text,list_status))
    df = pd.DataFrame.from_dict(features)
    df.to_csv(CSV_TEXT_DATASET_FILE, na_rep=0, index=False, header=True)

def load_dataset(file=CSV_EVEN_FILE):
    """
    loads the dataset file and return the X,Y arrays and the list of collums name
    :return: X,Y arrays and the list of collums name
    """
    df = pd.read_csv(file,header=0)
    print("sample")
    new_df = df.sample(frac=1).reset_index(drop=True)
    Y = new_df.pop("cve")
    X = new_df.values
    print(len(X[0]))

    return X,Y,new_df.columns

def load_even_dataset(file=CSV_EVEN_FILE):
    """
    loads a dataset of even classes repartition
    :param file: the name of the csv file
    :return: X,Y arrays and the list of collums name
    """
    df = pd.read_csv(file, header=0)
    data = df.values
    if file == CSV_EVEN_FILE:
        dataset = data
    else:
        data_VCC = [commit for commit in data if commit[0]]
        data_other = [commit for commit in data if not commit[0]]

        data_other = random.sample(data_other, len(data_VCC))
        dataset = data_VCC + data_other
        random.shuffle(dataset)
    Y = [commit[0] for commit in dataset]
    X = [commit[1:] for commit in dataset]


    return X,Y,df.columns

def make_even_dataset(file=CSV_FILE,output=CSV_EVEN_FILE):
    """
    Compute the even dataset from the complete one (ie it does downsampling
    :param file:
    :param output:
    :return:
    """
    df = pd.read_csv(file, header=0)
    data = df.values
    data_VCC = [commit for commit in data if commit[0]]
    data_other = [commit for commit in data if not commit[0]]
    print(len(data_other))
    data_other = random.sample(data_other, len(data_VCC))
    dataset = data_VCC + data_other
    new_dataframe = pd.DataFrame(dataset,columns=df.columns)

    #delete unused variables and cals the garbage collector
    del data_VCC
    del data_other
    del dataset
    del df
    del data
    gc.collect()


    final_df = compute_vectorizer_features(new_dataframe)
    final_df.to_csv(output,na_rep=0,index=False,header=True)



if __name__ == "__main__":
    db_to_dataset()#compute the first dataset
    make_even_dataset()#compute the even dataset