import re

from src.Machine_learning.Feature_engeniering.feature_info import *


def get_text_features_names(columns):
    r = re.compile("message_.*")
    message_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_content_.*")
    content_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_bef_.*")
    before_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_aft_.*")
    after_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_mixed_.*")
    mixed_feature = list(filter(r.match, list(columns)))
    r = re.compile("comment_.*")
    comment_feature = list(filter(r.match, list(columns)))
    r = re.compile("keyword_.*")
    keyword_feature = list(filter(r.match, list(columns)))
    features = []

    features.append(message_feature)
    features.append(content_feature)
    features.append(VCC_WORDS)
    features.append(before_feature+after_feature+mixed_feature)
    features.append(comment_feature)
    features.append(keyword_feature)

    return features

def get_features_id(columns):
    """
    :returns the features id regrouped by types
    :param columns: the list if features name
    :return: a list that contains the features id regrouped by type
    """
    r = re.compile(r"Comp_.*")
    comp_features = list(filter(r.match, list(columns)))
    r = re.compile("message_.*")
    message_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_content_.*")
    content_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_bef.*")
    before_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_aft_.*")
    after_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_mixed_.*")
    mixed_feature = list(filter(r.match, list(columns)))
    r = re.compile("comment_.*")
    comment_feature = list(filter(r.match, list(columns)))
    r = re.compile("keyword_.*")
    keyword_feature = list(filter(r.match, list(columns)))
    features_id = []
    features_id.append(list(columns.get_indexer(VCC+VCC_WORDS+content_feature+message_feature)))
    features_id.append(list(columns.get_indexer(BOSU)))
    features_id.append(list(columns.get_indexer(before_feature+after_feature+mixed_feature)))
    features_id.append(list(columns.get_indexer(RECENT)))
    features_id.append(list(columns.get_indexer(TIME)))
    features_id.append(list(columns.get_indexer(RATIO)))
    features_id.append(list(columns.get_indexer(CHANGES+comp_features)))
    features_id.append(list(columns.get_indexer(COMMENT+comment_feature+keyword_feature)))
    return features_id

def get_final_features_id(columns):
    """
    gets the list of final features selected after the features selection
    :param columns: the list if features name
    :return: a list of the features id
    """
    return get_final_nontext_features_id(columns)+get_final__text_features_id(columns)

def get_final_nontext_features_id(columns):
    """
    get the list if the final numerical features as a result of the statistical analysis
    :param columns: the list if features name
    :return: a list of the features id
    """
    features_name = VCC+BOSU+RECENT+TIME+RATIO+CHANGES + COMMENT
    features_id = list(columns.get_indexer(features_name))
    return features_id

def get_final__text_features_id(columns):
    """
    get the list of the final list of text features before the feature selection
    :param columns: the list if features name
    :return: a list of the features id
    """
    r = re.compile("message_.*")
    message_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_bef.*")
    before_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_aft_.*")
    after_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_mixed_.*")
    mixed_feature = list(filter(r.match, list(columns)))
    r = re.compile("comment_.*")
    comment_feature = list(filter(r.match, list(columns)))
    r = re.compile("keyword_.*")
    keyword_feature = list(filter(r.match, list(columns)))
    features_name = VCC_WORDS+message_feature +  before_feature + after_feature + mixed_feature  + comment_feature +keyword_feature
    features_id = list(columns.get_indexer(features_name))
    return features_id
def get_VCC_features_id(columns):
    """
    get the list of the list of features from the VCCFinder
    :param columns: the list if features name
    :return: a list of the features id
    """
    r = re.compile("message_.*")
    message_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_content_.*")
    content_feature = list(filter(r.match, list(columns)))
    features_name = VCC+VCC_WORDS+message_feature+content_feature
    features_id = list(columns.get_indexer(features_name))
    return features_id


