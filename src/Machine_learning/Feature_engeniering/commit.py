import regex
from sklearn.feature_extraction.text import CountVectorizer
from unidiff import PatchSet

EMPTY = 0
CODE = 1
DELLETION = 2
ADDITION =3
MODIFICATION = 4



def get_moddification(commit):
    """
    get the information from the commit in unidiff format
    :param commit: the content of the commit
    :return: a dictionary containing the properties of the commit with key:
        content: the commit as it is
        previous: the state of the code before the commit (only the code in the hunk)
        next: the state of the code after the commit (only the code in the hunk)
        addition: a list of added lines
        delletion: a list of removed lines
        nbrfile: the number of files affected by the commit
        nbrfunction: the number of hunks in the commit
        nbr_added_files: the numebr of fikes added by the commit
        nbr_deleted_files: the numebr of fikes removed by the commit
        nbr_modified_files: the numebr of fikes modified by the commit
        nbr_addition: the number of added lines by the commit (the number of line starting with a + in unidiff format)
        nbr_delletion: the number of removed lines by the commit (the number of line starting with a - in unidiff format)
        commit_size: the total number of code lines in the commit
    """

    info = {"content":commit, "previous":"", "next":"","addition":[], 'delletion':[],'nbrfile':0,'nbrfunction':0,'nbr_added_files':0,'nbr_deleted_files':0,'nbr_modified_files':0, 'nbr_addition':0, "nbr_delletion":0,"commit_size":0}
    patch = PatchSet(commit)
    info["nbr_delletion"] = patch.removed
    info["nbr_addition"] = patch.added
    info["nbrfile"] = len(patch)
    info['nbrfunction'] = sum([len(f) for f in patch])
    info['nbr_added_files'] = len(patch.added_files)
    info['nbr_deleted_files'] = len(patch.removed_files)
    info['nbr_modified_files'] = len(patch.modified_files)
    info['commit_size'] = sum([sum([len(hunk) for hunk in file]) for file in patch])
    previous = ""
    next =""
    addition = []
    delletion =[]
    for file in patch:
        for hunk in file:
                previous += "\n".join(hunk.source)
                next += "\n".join(hunk.target)
                addition.extend([str(line) for line in hunk if line.is_added])
                delletion.extend([str(line) for line in hunk if line.is_removed])
    info['previous'] = previous
    info['next']=next
    info['addition'] = addition
    info['delletion'] = delletion



    return info



def get_nbr_function_call(text):
    """
    compute the number of function call in the code states
    :param text: the code states
    :return: nbr of function call
    """
    nbr_calls =0
    p_regex = regex.compile('(?>[a-zA-Z0-9]{1,15}?\()(?>(?>[^()](?!\\n))|(?R))*?\)(?!{)')
    function_calls = p_regex.findall(text)
    nbr_calls = nbr_calls+len(function_calls)
    for function in function_calls:
        start = function.index("(")+1
        if len(function[start:-1]) >2:
            nbr_calls += get_nbr_function_call(function[start:-1])

    return nbr_calls



def remove_comment(commit,return_comment = False):
    """
    return the commmit with the comments removed
    :param commit: A string that corespond to a commit unidiff format
    :param return_comment: A Boolean if true we also return the commit
    :return: The commit with commit removed and if return_comment is True it return a string that contains the comments
    """
    if type(commit) == type(" "):
        new_commit = ""
        comment = ""
        for line in true_splitline(commit):
            try:
                if "#" in line:
                    comment_start = line.index("#")
                    new_commit += line[:comment_start]+ "\n"
                elif "//" in line:
                    comment_start = line.index("//")
                    new_commit += line[:comment_start] + "\n"
                elif "/*" in line:
                    comment_start = line.index("/*")
                    if "*/" in line:
                        comment_end = line.index("*/")
                        new_commit += line[:comment_start] + line[comment_end] +"\n"
                        comment += line[comment_start:comment_end] +"\n"
                    else:
                        new_commit += line[:comment_start] + "\n"
                        comment += line[comment_start:] + "\n"
                elif "*" in line:
                    comment_start = line.index("*")
                    if "*/" in line:
                        comment_end = line.index("*/")
                        if comment_start != comment_end:
                            new_commit += line[:comment_start] + line[comment_end] +"\n"
                            comment += line[comment_start:comment_end] +"\n"
                        else:
                            new_commit += line + "\n"
                    else:
                        new_commit += line[:comment_start] + "\n"
                        comment += line[comment_start:] + "\n"

                else:
                    new_commit+= line + "\n"
            except:
                print(line)
        if return_comment:
            return new_commit,comment
        else:
            return new_commit

    else:
        if return_comment:
            return "", ""
        else:
            return ""






def commit_to_vector(corpus):
    """
    Return the vectorized corpus of text
    :param corpus: a list of text
    :return: the features matrix and a list of the features name
    """
    try:
        vectorizer = CountVectorizer()
        features = vectorizer.fit_transform(corpus)

        return features,vectorizer.get_feature_names()
    except ValueError as error:

        print("corpus is empty:"+error)
        return None,None




def true_splitline(code):
    """this code is used to split the lines avoiding the \n in comments"""
    try:
        splited = regex.split(r'(?<!\\)\n', code)
        """for char in code:
            if char == '\"':
                string= not(string)
                if(id+1 < len(code)) and code[id+1] == "\\":
                    string = not(string)
            elif char == '\n':
                if not(string):
                    split.append(code[id_start:id_end])
                    id_start=id+1
                elif code[id+1] in ['+','-']:
                    split.append(code[id_start:id_end])
                    id_start = id + 1
            elif char == ";":
                string =False
            id_end += 1
            id += 1
        if id_start+1 != id_end:
            split.append(code[id_start:id_end-1])"""
    except Exception as e:
        print(e)
        print([code])
    return splited







