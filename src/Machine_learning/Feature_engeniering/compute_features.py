import gc

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import euclidean_distances
from sqlalchemy.dialects.postgresql import HSTORE

from Machine_learning.Feature_engeniering.commit import commit_to_vector, get_moddification, \
    remove_comment, get_nbr_function_call
from Machine_learning.Feature_engeniering.feature_info import VCC_WORDS
from src.database.database_info import PORT, PASSWORD, USERNAME, NEW_DB
from src.database.load_db import Database


def compute_vectorizer_features(new_dataframe):
    """
    Compute the Vector/text features from the complete dataframe
    :param new_dataframe: panda dataframe that contains the states of the code
    :return: The updated dataframe whit the features added
    """
    codes_previous = new_dataframe.pop("previous_content")
    codes_after = new_dataframe.pop("next_content")
    message_content = new_dataframe.pop("commit_text")

    # compute the various bag of words for the content of the commit and saves it to a dataframe
    code_states = []
    comments = []

    for code_state_before, code_state_after in zip(codes_previous, codes_after):
        code_bef, comment_bef = remove_comment(code_state_before, return_comment=True)
        code_states.append(code_bef)
        comments.append(comment_bef)
        code_aft, comment_aft = remove_comment(code_state_after, return_comment=True)
        code_states.append(code_aft)
        comments.append(comment_aft)
        code_states.append(code_state_before)
        code_states.append(code_state_after)

    text_compare, text_content = compare_feature(code_states, return_content=True)
    text_compare_df = pd.DataFrame.from_dict(text_compare)
    text_content_df = pd.DataFrame.from_dict(text_content)

    #computes the comment features
    comment_features = compare_feature(comments, "comment_")
    comment_df = pd.DataFrame.from_dict(comment_features)

    # Compute de message bag of words and saves it to a dataframe
    message_features = message_feature(list(message_content.values))
    message_df = pd.DataFrame.from_dict(message_features)


    #compute the keyword bag of words
    keyword_features = compare_feature(code_states, "keyword_", binary=False, vocabulary=VCC_WORDS)
    keyword_df = pd.DataFrame.from_dict(keyword_features)

    # cals the garbage colector in order to save memory
    gc.collect()

    # Regroup all the dataframes and saves them to a csv file
    final_df = pd.concat([new_dataframe, text_compare_df, text_content_df, message_df, comment_df,
     keyword_df], axis=1)
    return final_df

def make_ml_features(id,db_name):
    """
    create the features for all commits in the project with id
    :param id: the id of the project
    :type id: int
    :param db_name string coresponding to the database name
    :return: the features as a dictionary
    """
    new_db = Database(PORT, USERNAME, PASSWORD, db_name)
    if db_name == NEW_DB:
        commits = new_db.get_commits_ordered(id)
    else:
        commits = new_db.get_commits_ordered(id,export=True)
    total_new = 0



    if len(commits) > 0:
        VCC = False
        for commit in commits:
            if commit[3] == 'blamed_commit':
                VCC = True
        if not(VCC):
            print("repository does not contain VCC")
            return None
        #repository = new_db.get_repository(id,False)
        total_commits=len(commits)
        nbr_fail = 0
        temp_commits =0
        nbr_author=0
        authors = {}
        ids=[]
        ml_features_dict = {}
        text_feature_data = {}
        to_update = []
        commit_author = {}
        number_changes =0
        initial_time = commits[0][8]
        recent_commits=[]
        update = 0
        for commit in commits:
            if not(commit[0] in ids):

                ids.append(commit[0])
                temp_commits += 1
                commit_author[commit[0]] = commit[7]

                #compute the authors
                if commit[7] in authors:
                    info_author = authors[commit[7]]
                    info_author[0][id] +=1
                    authors[commit[7]] = info_author
                else:
                    nbr_author+=1
                    info_author = [{id:1}, commit[7]]
                    authors[commit[7]] = info_author



                number_changes +=1
                try:
                    content_info = get_moddification(commit[21])
                    patch_keywords = commit[25]

                    # get the patch keywords from the database
                    f = HSTORE().result_processor(None, None)
                    keyword_feature = f(patch_keywords)

                    corpus = [content_info["previous"], content_info["next"]]
                    features, features_name = commit_to_vector(corpus)
                    new_corpus = []
                    comment_corpus = []
                    for code in corpus:
                        content, comment = remove_comment(code,return_comment=True)
                        new_corpus.append(content)
                        comment_corpus.append(comment)
                    complexity_features = get_complexity_features(new_corpus)
                    comment_features= {"size_comment_bef":len(comment_corpus[0]),"size_comment_after":len(comment_corpus[1]),"size_comment_dif":len(comment_corpus[1])-len(comment_corpus[0])}


                    if type(features) == type(None):
                        euclidean_distance = 0
                    else:
                        euclidean_distance = euclidean_distances(features[0], features[1])[0][0]
                        # get bag of words for after before and mixed



                    cve = False
                    # stores the id of the blammed commit
                    if commit[3] == 'blamed_commit':
                        cve = True
                    elif commit[23] != '':
                        if commit[2] != None:
                            if commit[2] == commit[0]:
                                cve = True
                            elif commit[2] in ids:
                                if not (ml_features_dict[commit[2]]["cve"]):
                                    total_new += 1
                                ml_features_dict[commit[2]]["cve"] = True
                                text_feature_data[commit[2]]["cve"] = True
                            else:
                                update += 1
                                to_update.append(commit[2])

                    else:
                        if commit[3] != 'other_commit':
                            print(commit[3])

                    VCC_features = {"number_of_commit": total_commits, "Number_of_unique_contributor": 0,
                                    "contribution_in_project": 0,
                                    "Additions": content_info['nbr_addition'],
                                    "Deletions": content_info["nbr_delletion"],
                                    "Past_changes": temp_commits, "Future_changes": total_commits - temp_commits,
                                    "Past_different_authors": nbr_author, "future_different_authors": 0,
                                    "Hunk_count": content_info["nbrfunction"]}

                    # set the various variable
                    nbr_commit_author = authors[commit[7]][0][id]
                    author_percent = nbr_commit_author / temp_commits

                    # check if the author did commit
                    if commit[7] == commit[10]:
                        author_commiter = 1
                    else:
                        author_commiter = 0

                    bosu_features = {"commit_size": content_info["commit_size"], "nbr_files": content_info['nbrfile'],
                                     "nbr_added_files": content_info["nbr_added_files"],
                                     "nbr_delleted_files": content_info['nbr_deleted_files'],
                                     "nbr_modified_files": content_info['nbr_modified_files'],
                                     "nbr_commit_author": authors[commit[7]][0][id],
                                     "author_commit_percent": author_percent, "author_commiter": author_commiter}


                    changes_feature = {"euclidian_distance": euclidean_distance}
                    changes_feature.update(complexity_features)

                    time_feature = compute_time_features(commit[8],commit[11],initial_time)

                    recent_XD_features,recent_commits = compute_recent_features(recent_commits,commit[7],commit[8],content_info["commit_size"])

                    ratio_features = compute_ratio_features(content_info)




                    # compute the number of recent commit

                    text = {"previous_content": content_info["previous"], "next_content": content_info["next"],"commit_text":commit[20]}
                    features = {"cve": cve}
                    VCC_features.update(keyword_feature)

                    features.update(comment_features)
                    features.update(changes_feature)
                    features.update(ratio_features)
                    features.update(time_feature)
                    features.update(recent_XD_features)
                    features.update(bosu_features)
                    features.update(VCC_features)
                    features.update(text)
                    ml_features_dict[commit[0]] = features
                except Exception as e:
                    if not("Hunk" in str(e)):
                        print(e)
                    nbr_fail+=1
                    ids.remove(commit[0])


        print("number of error:"+str(nbr_fail))
        new_db.close_db()

        print("nomber of commit to update"+str(update))
        print(total_new)
        nbr_total_author = len(authors.values())
        list_id_status = []
        for commit_id in ids:
            author = commit_author[commit_id]
            nbr_commit = authors[author][0][id]
            ml_features_dict[commit_id]["Number_of_unique_contributor"] = nbr_total_author
            ml_features_dict[commit_id]["contribution_in_project"] = nbr_commit/total_commits
            ml_features_dict[commit_id]["future_different_authors"] = nbr_total_author - ml_features_dict[commit_id]["Past_different_authors"]

            if commit_id in to_update:
                update -= 1
                ml_features_dict[commit_id]["cve"] = True
            list_id_status.append([commit_id,ml_features_dict[commit_id]["cve"]])

        print("nomber of commit to update after"+str(update))
        return list(ml_features_dict.values())

    else:
        print("no data for id: %s"%(str(id)))
        return None
def compute_ratio_features(content_info):
    """
    Computes the ratio features for the specific commit
    :param content_info: the dictionary that contains the information about the content of the commit
    :return: a dictionary that contains all the ration features
    """
    ratio_features ={"nbr_add/nbr_del_ratio": 0}
    if (content_info["nbr_delletion"] != 0):
        ratio_features["nbr_add/nbr_del_ratio"] = content_info['nbr_addition'] / content_info[
            "nbr_delletion"]
    if (content_info["commit_size"] != 0):
        ratio_features["nbr_add/commit_size_ratio"] = content_info["nbr_addition"] / content_info[
            "commit_size"]
    if (content_info["nbrfile"] != 0):
        ratio_features["nbr_added_files_ratio"] = content_info["nbr_added_files"] / content_info[
            'nbrfile']
        ratio_features["nbr_delleted_files_ratio"] = content_info['nbr_deleted_files'] / content_info[
            'nbrfile']
        ratio_features["nbr_modified_files_ratio"] = content_info['nbr_modified_files'] / content_info[
            'nbrfile']
    return ratio_features

def compute_time_features(commit_time,publish_time,initial_time):
    """
    Compute the time based features
    :param commit_time: the time the commit was made
    :param publish_time: the time the commit was validated and published
    :param initial_time: the time of the first commit in the repo
    :return: a dictionary that contain the features
    """
    time = (commit_time.time().hour * 60) + (commit_time.time().minute)
    time_commit = (commit_time - initial_time).total_seconds()
    time_to_publish = (publish_time - commit_time).total_seconds()

    time_feature = {"time_published": time, "time_before_commit": time_commit,
                    "time_to_publish": time_to_publish, "published_0To6": 0, "published_6To12": 0,
                    "published_12to18": 0, "published_18To24": 0}

    hour_published = commit_time.time().hour
    if hour_published < 6:
        time_feature["published_0To6"] = 1
    elif hour_published < 12:
        time_feature["published_6To12"] = 1
    elif hour_published < 18:
        time_feature["published_12to18"] = 1
    else:
        time_feature["published_18To24"] = 1
    return time_feature

def compute_recent_features(recent_commits,author,date,commit_size):
    """
    Compute all the "Recent" features
    :param recent_commits: a list of dictionaries that contain information about recent commits
    :param author: The author of the new commit
    :param date: the date of the new commit
    :param commit_size: the size of the new commit
    :return: A dictionary that contains the features, the updated recent_commits list
    """
    recent_XD_features = {"recent_30D_commit": 0, "recent_30D_commit_size": 0, "recent_20D_commit": 0,
                          "recent_20D_commit_size": 0, "recent_10D_commit": 0,
                          "recent_10D_commit_size": 0, "recent_5D_commit": 0,
                          "recent_5D_commit_size": 0}

    recent_authors = [author]
    nbr_recent_commit_author = 0
    for recent_commit in recent_commits:
        commit_days = (date - recent_commit["date"]).days
        if commit_days > 40:
            recent_commits.remove(recent_commit)
        elif commit_days <= 40:
            if not (recent_commit["author"] in recent_authors):
                recent_authors.append(recent_commit["author"])
            if recent_commit["author"] == author:
                nbr_recent_commit_author += 1
        if commit_days < 30:
            recent_XD_features["recent_30D_commit"] += 1
            recent_XD_features["recent_30D_commit_size"] += recent_commit["size"]
        if commit_days < 20:
            recent_XD_features["recent_20D_commit"] += 1
            recent_XD_features["recent_20D_commit_size"] += recent_commit["size"]
        if commit_days < 10:
            recent_XD_features["recent_10D_commit"] += 1
            recent_XD_features["recent_10D_commit_size"] += recent_commit["size"]
        if commit_days < 5:
            recent_XD_features["recent_5D_commit"] += 1
            recent_XD_features["recent_5D_commit_size"] += recent_commit["size"]

    recent_XD_features["recent_40D_commit"] = len(recent_commits)
    recent_XD_features["recent_40D_commit_size"] = sum([com["size"] for com in recent_commits])
    recent_XD_features["recent_40D_authors"] = len(recent_authors)
    recent_XD_features["recent_40D_commit_author"] = nbr_recent_commit_author
    if len(recent_commits) != 0:
        recent_XD_features["recent_40D_commit_author_pourcentage"] = nbr_recent_commit_author / len(
            recent_commits)
    else:
        recent_XD_features["recent_40D_commit_author_pourcentage"] = 0
    recent_commits.append({"date": date, "size": commit_size, "author": author})
    return recent_XD_features,recent_commits


def compare_feature(commit, feature_group_name ="code_",binary=True,vocabulary=None,return_content =False):
    """
    Compute the the text features for the commits
    :param commit:  the list of commits
    :param feature_group_name: the name of the features type in order to save it
    :param binary: state if it produce bag of words or count the number of occurance of the terms
    :param vocabulary:a list of words to be used for the vectorizer
    :param return_content: if the function must return the content
    :return: a dictionary that contains the computed features
    """
    vectorizer = CountVectorizer(binary=binary,max_df=0.995,min_df=0.005,token_pattern=r"(?u)\b[a-zA-Z][a-zA-Z]+\b",vocabulary=vocabulary)
    for id in range(len(commit)):
        if type(commit[id]) != type(" "):
            print(commit[id])
            commit[id] =""
    features = vectorizer.fit_transform(commit)
    features_name = vectorizer.get_feature_names()
    result =[]
    content_result = []
    for commit_id in range(0,len(list(features)),2):
        tmp_result = {}
        total = {}
        before = {}
        after = {}
        mixed = {}
        for i in range(len(features_name)):


            before[feature_group_name+'bef_' + features_name[i]] = features[commit_id, i]
            after[feature_group_name+'aft_' + features_name[i]] =features[commit_id+1, i]
            if binary:
                if (features[commit_id, i] != 0 and features[commit_id+1 , i] != 0):
                    mixed[feature_group_name+'mixed_' + features_name[i]] = 1
                    total[feature_group_name+'content_' + features_name[i]] = 1
                else:
                    total[feature_group_name+'content_' + features_name[i]] = features[commit_id, i]+features[commit_id+1, i]
            else:
                mixed[feature_group_name + 'diff_' + features_name[i]] = after[feature_group_name+'aft_'+features_name[i]]-before[feature_group_name+'bef_' + features_name[i]]

        tmp_result.update(before)
        tmp_result.update(after)
        tmp_result.update(mixed)
        result.append(tmp_result)
        content_result.append(total)
    if return_content:
        return result,content_result
    else:
        return result

def message_feature(message):
    """
    Compute the message bag of words
    :param message: a list of all the commit messages
    :return: a list of all the dictionary for the features
    """
    vectorizer = CountVectorizer(binary=True,max_df=0.995,min_df=0.005,token_pattern=r"(?u)\b[a-zA-Z][a-zA-Z]+\b")
    for id in range(len(message)):
        if type(message[id]) != type(" "):
            message[id] = ""
    features = vectorizer.fit_transform(message)
    features_name = vectorizer.get_feature_names()
    message = []
    for feature_id in range(len(list(features))):
        tmp_message = {}
        for i in range(len(features_name)):
            tmp_message['message_'+features_name[i]] =features[feature_id,i]
        message.append(tmp_message)
    return message



def text_feature(list_commit,list_status):
    """
    Compute the bag of words for the content of the commit
    :param list_commit: a list of commit (before and after)
    :param list_status: a list of bool that states if the commit is a VCC or not
    :return: a list of of all the dictionary containing the featuress
    """
    try:
        vectorizer = CountVectorizer(binary=True)
        features = vectorizer.fit_transform(list_commit)

        features_name = vectorizer.get_feature_names()
    except ValueError as error:

        print("corpus is empty:" + error)
        return None
    list_result = []
    for commit_id in range(0,2*len(list_status),2):
        true_id= int(commit_id/2)
        before = {}
        after = {}
        mixed = {}
        for i in range(len(features_name)):
            if (features[commit_id, i] != 0 and features[commit_id+1, i] != 0):
                mixed['mixed_' + features_name[i]] = features[commit_id, i] + features[commit_id+1, i]

            before['bef_' + features_name[i]] = features[commit_id, i]
            after['aft_' + features_name[i]] = features[commit_id+1, i]
        dict_text_feature = {'cve':list_status[true_id]}
        dict_text_feature.update(mixed)
        dict_text_feature.update(before)
        dict_text_feature.update(after)

        list_result.append(dict_text_feature)
    return list_result

def get_compare_ocurances(corpus,voc):
    """
    Return the feature that compute
    comparison of occurrence of somme type of term and keyword
    Edit: this is not used and replace by key-word compare features
    :param voc: the vacabulary to use
    :return:the dictionary of the result
    """
    compare_features= {}
    complexity_vectorizer = CountVectorizer(token_pattern=r"(?u)\b[a-zA-Z][a-zA-Z]+\b",
                                            vocabulary= voc)
    features = complexity_vectorizer.fit_transform(corpus)
    for i in range(len(voc)):
        compare_features["Comp_bef_"+ voc[i]] = features[0,i]
        compare_features["Comp_aft_"+ voc[i]] = features[1,i]
        compare_features["Comp_diff_"+voc[i]] = compare_features["Comp_aft_"+ voc[i]]- compare_features["Comp_bef_"+ voc[i]]
    return compare_features

def get_complexity_features(corpus):
    """
    compute the complexity features
    :param corpus: The list of text on which the features will be computed
    :return: the dictionary of the features
    """
    complexity_features = {}
    """
    #replaced by key word features
    voc = ["for", "switch", "while", "do","if", "case"]
    get_compare_ocurances(corpus,voc)
    """
    complexity_features["Comp_functcall_bef"] = get_nbr_function_call(corpus[0])
    complexity_features["Comp_functcall_aft"] = get_nbr_function_call(corpus[1])
    complexity_features["Comp_functcall_diff"] = complexity_features["Comp_functcall_aft"] - complexity_features["Comp_functcall_bef"]
    return complexity_features

