LAST_FEATURE =["number_of_commit","commit_size","recent_30D_commit","time_published","nbr_add/nbr_del_ratio","euclidian_distance"]

COMMENT = ["size_comment_bef","size_comment_after","size_comment_dif"]
CHANGES = ["euclidian_distance","Comp_functcall_diff","Comp_functcall_aft","Comp_functcall_bef"]
RATIO = ["nbr_add/nbr_del_ratio","nbr_add/commit_size_ratio","nbr_added_files_ratio"]
TIME = ["time_to_publish"]
RECENT = ["recent_40D_commit","recent_40D_commit_size","recent_40D_authors","recent_40D_commit_author","recent_40D_commit_author_pourcentage"]
BOSU = ["commit_size","nbr_files","nbr_added_files","nbr_modified_files","nbr_commit_author","author_commit_percent","author_commiter"]
VCC = ["number_of_commit","Number_of_unique_contributor","contribution_in_project","Additions","Deletions","Past_changes","Future_changes","Past_different_authors","future_different_authors","Hunk_count"]
VCC_WORDS = ["do","if","asm","for","int","new","try","auto","bool","case","char","else","enum","goto","long","this","true","void","break","catch","class","const","false","float","short","throw","union","using","while","delete","double","extern","friend","inline","public","return","signed","sizeof","static","struct","switch","typeid","default","mutable","private","typedef","virtual","wchar_t","continue","explicit","operator","register","template","typename","unsigned","volatile","namespace","protected","const_cast","static_cast","dynamic_cast","reinterpret_cast","previous_content","next_content","total_content","commit_text","free","alloc","alloca","calloc","malloc","realloc"]
FEATURE_TO_TEST= ["Future_changes","future_different_authors","nbr_delleted_files","nbr_commit_author",  "nbr_add/nbr_del_ratio","recent_40D_commit_author_pourcentage", "time_published"]


PACK_FEATURES = [VCC+VCC_WORDS,BOSU,RECENT,TIME,RATIO,CHANGES,COMMENT]
PACK_FEATURE_NAME = ["VCC", "bosu","bag_of_words", "recent", "time", "ratio", "changes","comment"]


NON_TEXT_PACK_FEATURES = [VCC,BOSU,RECENT,TIME,RATIO,CHANGES,COMMENT]

NON_TEXT_PACK_FEATURE_NAME = ["VCC", "bosu", "recent", "time", "ratio", "changes","comment"]
