import re

import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC, SVC

from Machine_learning.Dataset.dataset import load_dataset
from Machine_learning.Feature_engeniering.feature_info import FEATURE_TO_TEST, VCC_WORDS
from Machine_learning.Feature_engeniering.get_feature import get_final__text_features_id, get_final_nontext_features_id
from Machine_learning.Tools.compare_evolution import evolution_score
from Machine_learning.Tools.compare_features import features_score, compare_duplicate_features_score, compare_score
from paths import CSV_EVEN_FILE, PROJECT_ROOT


def test_features_score():
    """
    Test somme packs of features and print the result
    :return:None
    """
    scoring = ['accuracy', 'precision', 'recall', 'f1']
    clf = LinearSVC(max_iter=10000)
    result = features_score(clf,scoring,FEATURE_TO_TEST)
    print(result.to_latex())



def test_text_features():
    """
    Test
    :return: None
    """
    X, Y, columns = load_dataset()
    features_id = []

    r = re.compile("code_bef.*")
    before_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_aft_.*")
    after_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_mixed_.*")
    mixed_feature = list(filter(r.match, list(columns)))
    features_name = before_feature + after_feature + mixed_feature
    features_id.append(list(columns.get_indexer(features_name)))

    r = re.compile("message_.*")
    message_feature = list(filter(r.match, list(columns)))
    features_name = message_feature
    features_id.append(list(columns.get_indexer(features_name)))

    r = re.compile("comment_.*")
    comment_feature = list(filter(r.match, list(columns)))
    features_name = comment_feature
    features_id.append(list(columns.get_indexer(features_name)))



    """r = re.compile("code_content_.*")
    content_feature = list(filter(r.match, list(columns)))
    features_name = content_feature
    features_id.append(list(columns.get_indexer(features_name)))"""

    r = re.compile(r"Comp_.*")
    comp_features = list(filter(r.match, list(columns)))
    features_name = comp_features
    features_id.append(list(columns.get_indexer(features_name)))

    r = re.compile("keyword_.*")
    keyword_feature = list(filter(r.match, list(columns)))
    features_name = keyword_feature
    features_id.append(list(columns.get_indexer(features_name)))

    X = StandardScaler().fit_transform(X)
    clf = LinearSVC(max_iter=100000)
    result = evolution_score(X,Y,columns,clf, ['accuracy', 'precision', 'recall', 'f1'],features_id,["code_states","message","comment","complexity","keyword"])
    print(result.to_latex())
def test_duplicate_bow_features():
    """
    Test the dublicates bag of wors features described in the paper
    and print the latex table of the resuls
    :return: None
    """
    scoring = ['accuracy', 'precision', 'recall', 'f1']
    clf = LinearSVC(max_iter=10000)
    X, Y, columns = load_dataset()
    r = re.compile("code_content_.*")
    content_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_bef_.*")
    before_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_aft_.*")
    after_feature = list(filter(r.match, list(columns)))
    r = re.compile("code_mixed_.*")
    mixed_feature = list(filter(r.match, list(columns)))
    content_state_feature = before_feature+after_feature+mixed_feature
    print(compare_duplicate_features_score(X,Y,columns,clf,scoring,content_feature,content_state_feature).to_latex())


def test_duplicate_keyword_features():
    """
    Test the dublicates keywords features described in the paper
    and print the latex table of the resuls
    :return:
    """
    scoring = ['accuracy', 'precision', 'recall', 'f1']
    clf = LinearSVC(max_iter=500000,tol=1e-6)
    X, Y, columns = load_dataset()
    r = re.compile("keyword_.*")
    keyword_feature = list(filter(r.match, list(columns)))
    print(compare_duplicate_features_score(X, Y, columns, clf, scoring, VCC_WORDS, keyword_feature).to_latex())

def test_textVSNumerical_features(dataset_name=CSV_EVEN_FILE):
    """
    Compare the result of numerical features vs the result on text features
    on a specific dataset and print tje resulting latex table
    :param dataset_name: the name of the dataset
    :return: None
    """
    X, Y, columns = load_dataset(dataset_name)
    Text_features_ids = get_final__text_features_id(columns)
    numerical_features_ids = get_final_nontext_features_id(columns)
    X_text = [item[Text_features_ids] for item in X]
    X_numerical = [item[numerical_features_ids] for item in X]
    X_total = [item[numerical_features_ids+Text_features_ids] for item in X]
    scoring = ['accuracy', 'precision', 'recall', 'f1']
    clf = make_pipeline(StandardScaler(),LinearSVC(random_state=0,max_iter=50000))
    print(compare_score([X_text,X_numerical,X_total],[Y,Y,Y],["vector","numerical","both","final"],clf,scoring).to_latex())

def show_top_features(clf,columns):
    """
    plot a table showing the top 30 features of the classifier
    :param clf: the scikit learn classifier
    :param columns: the list of the features names
    :return: None
    """
    feature_importance = np.array(list(map(abs ,clf.coef_[0])))
    feature_names =columns
    sorted_id = feature_importance.argsort()[-30:]
    y_ticks = np.arange(0, len(sorted_id))
    names=feature_names[sorted_id]
    fig, ax = plt.subplots()
    ax.barh(y_ticks, feature_importance[sorted_id])
    ax.set_yticklabels(names)
    ax.set_yticks(y_ticks)
    ax.set_title("Top 30 Features coefficient")
    plt.show()

def show_final_top_features():
    clf =  SVC(kernel='linear', C=0.1, tol=0.01)
    X, Y, columns_final = load_dataset(PROJECT_ROOT / "output" / "dataset" / 'final_dataset.csv')
    X_scaled = StandardScaler().fit_transform(X)
    X_train, X_test, y_train, y_test = train_test_split(X_scaled, Y, train_size=0.75, test_size=0.25,
                                                                        random_state=101)
    clf.fit(X_train,y_train)
    y_pred = clf.predict(X_test)
    print(accuracy_score(y_test,y_pred))
    show_top_features(clf,columns_final)






if __name__ == "__main__":


    test_textVSNumerical_features() #to show the comparaison before the features otpimization
    test_textVSNumerical_features(PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv') #to show the comparaison after the features otpimization

    show_final_top_features()







