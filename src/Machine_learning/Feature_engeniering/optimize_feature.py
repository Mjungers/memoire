import matplotlib.pyplot as plt
import pandas as pd
from sklearn import decomposition
from sklearn.feature_selection import RFECV
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.model_selection import KFold
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.svm import LinearSVC, SVC

from Machine_learning.Dataset.dataset import load_dataset
from Machine_learning.Feature_engeniering.get_feature import get_final__text_features_id, get_final_nontext_features_id
from Machine_learning.Tools.compare_features import compare_score
from paths import PROJECT_ROOT


def features_PCA():
    """
    compare the result of the algorithm before and after using the PCA algorithm
    :return:
    """
    X, Y, columns = load_dataset()
    pca = decomposition.PCA()
    X2 = pca.fit_transform(X)
    scoring = ['accuracy', 'precision', 'recall', 'f1']
    clf = make_pipeline(StandardScaler(),LinearSVC())
    print(compare_score([X,X2],[Y,Y],["Normal","PCA"],clf,scoring).to_latex())

def compute_optimal_dataset(output=PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv'):
    """
    Compute and saves the optimal dataset by removing the features
    from the result of the features selection algorithm
    :param output: the name of the file to save the result
    :return: None
    """
    X, Y, columns = load_dataset()
    Text_features_ids = get_final__text_features_id(columns)
    numerical_features_ids = get_final_nontext_features_id(columns)
    X_text = [item[Text_features_ids] for item in X]

    features_selected = features_elimination(X_text,Y)
    selected_text_features_id = [feauture_id for (feauture_id,state) in zip(Text_features_ids,features_selected) if state]
    X_final = [item[numerical_features_ids+selected_text_features_id] for item in X]
    final_dataframe = pd.DataFrame(X_final,columns=columns[numerical_features_ids+selected_text_features_id])
    final_dataframe.insert(0,value=Y,column="cve")
    final_dataframe.to_csv(output,na_rep=0,index=False,header=True)
def features_elimination(X,Y,plot=True):
    """
    Computes the optimal number of features for the algorithm
    and plot the results of the test
    From:https://scikit-learn.org/stable/auto_examples/feature_selection/plot_rfe_with_cross_validation.html#sphx-glr-auto-examples-feature-selection-plot-rfe-with-cross-validation-py
    :return: The selection of features
    """
    step = 100
    start = 100
    X = StandardScaler().fit_transform(X)
    clf = SVC(kernel="linear")

    kfold = KFold(n_splits=10)
    rfecv=RFECV(estimator=clf,step=step,cv=kfold,scoring='accuracy',min_features_to_select=start)
    rfecv.fit(X, Y)
    print("Optimal number of features : %d" % (rfecv.n_features_))
    print(rfecv.grid_scores_)

    if plot:
        # Plot number of features VS. cross-validation scores
        plt.figure()
        plt.xlabel("Number of features selected")
        plt.ylabel("Cross validation score (nb of correct classifications)")
        plt.plot(range(start,
                       len(rfecv.grid_scores_)*step + start,step),
                 rfecv.grid_scores_)
        plt.show()
    return rfecv.get_support()

def features_select_kbest(output=PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv'):
    """
    Select the k best features (not used as it gives poor result)
    :param output: the name of the file to save the dataset
    :return:None
    """
    X, Y, columns = load_dataset()
    Text_features_ids = get_final__text_features_id(columns)
    numerical_features_ids = get_final_nontext_features_id(columns)
    X_text = [item[Text_features_ids] for item in X]
    X_text = MinMaxScaler().fit_transform(X_text)
    selector = SelectKBest(score_func=chi2, k=5395).fit(X_text, Y)
    result = list(selector.get_support(indices=False))
    selected_text_features_id = list(columns.get_indexer(result))
    X_final = [item[numerical_features_ids + selected_text_features_id] for item in X]
    final_dataframe = pd.DataFrame(X_final, columns=columns[numerical_features_ids + selected_text_features_id])
    final_dataframe.insert(0, value=Y, column="cve")
    final_dataframe.to_csv(output, na_rep=0, index=False, header=True)