import pandas as pd
from numpy import mean, std
from sklearn.model_selection import KFold, cross_validate
from sklearn.preprocessing import StandardScaler

"""
These function can compare the various evolution of the result of an algorithm when adding new features
"""


def compare_score(X,Y,clasifiers,scoring,scaller = True):
    """
    return  the scores result of the clasifiers can be used to compare algorithm
    :param clf: a list of tuple with scikit learn clasifier  (classifier, name)
    :param scoring: a list or dict of the score used to compare see cross_validate scoring
    :param whether to use a scaler or not
    :return: panda dataframe of the result
    """
    if scaller:
        scaler = StandardScaler().fit(X)
        X = scaler.transform(X)
    stats = {}
    for (name,clf) in clasifiers:
        kfold = KFold(n_splits=10)
        scores_result = cross_validate(clf,X,Y,scoring=scoring,cv=kfold,n_jobs=-1)
        stats_temps={}
        stats_temps["fit_time"] = mean(list(scores_result['fit_time']))
        stats_temps["score_time"] = mean(list(scores_result['score_time']))
        for score_name in scoring:
            stats_temps[score_name] = "%.3f (%.3f)"%(mean(list(scores_result['test_'+score_name])),std(list(scores_result['test_'+score_name])))
        stats[name]=stats_temps
    return pd.DataFrame.from_dict(stats)
