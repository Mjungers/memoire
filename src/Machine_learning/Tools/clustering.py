import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE

from paths import PROJECT_ROOT
from src.Machine_learning.Dataset.dataset import load_even_dataset, load_dataset


def Kmean():
    """
    test the k-means algorithm  and print the result
    :return: None
    """
    X,Y,collumns = load_dataset(PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv')
    y_pred = KMeans(n_clusters=2, random_state=150).fit_predict(X)
    print(y_pred)
    print(Y)
    result = {"true_cl1": 0,"true_cl0": 0,"false_cl1": 0,"false_cl0": 0}
    for i in range(len(Y)):
        if Y[i]:
            if y_pred[i] == 1:
                result["true_cl1"]  += 1
            else:
                result["true_cl0"] += 1
        else:
            if y_pred[i] == 1:
                result["false_cl1"] += 1
            else:
                result["false_cl0"] += 1
    print(result)

def TSNE_algo():
    """
    Execute the tsne algorithm on the dataset and plot the result
    :return:
    """
    n_neighbors = 10
    n_components = 2
    fig = plt.figure(figsize=(20, 16))
    fig.suptitle("Manifold Learning with %i points, %i neighbors"
                 % (1000, n_neighbors), fontsize=14)
    X, Y,collumns= load_dataset(PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv')
    colors = list(map(bool_to_color, Y))

    for i in range(1,41,5):
        method = TSNE(n_components=2, random_state=2, perplexity=i)
        Y = method.fit_transform(X)
        ax = fig.add_subplot(4, 5, 2 + int(i/5) + (int(i/5) > 3))
        ax.scatter(Y[:, 0], Y[:, 1], c=colors)
        ax.set_title("perplexity: %s" % (i))
        ax.axis('tight')
    plt.show()

def TSNE_per_file():
    """
    execute the tsne algorithm on specific repositories and plot the result
    :return: None
    """
    n_neighbors = 10
    n_components = 2
    fig = plt.figure(figsize=(15, 8))
    fig.suptitle("Manifold Learning with %i points, %i neighbors"
                 % (1000, n_neighbors), fontsize=14)


    methods_tSNE = TSNE(n_components=2,init='pca', random_state=0)
    i=0
    for file in ["181","189","242","260","315","327"]:
        X, Y = load_even_dataset(PROJECT_ROOT/"output"/"dataset"/file/'dataset.csv')
        if(len(X)!=0):
            colors = list(map(bool_to_color, Y))
            Y = methods_tSNE.fit_transform(X)
            ax = fig.add_subplot(2, 5, 2 + i + (i > 3))
            ax.scatter(Y[:, 0], Y[:, 1], c=colors)
            ax.set_title("%s" % (file))
            ax.axis('tight')
            i +=1
    plt.show()

def bool_to_color(bool):
    """
    convert the bool classication into a color
    :param bool: the classification
    :return: the collor
    """
    if bool:
        return '#FF0000'
    else:
        return '#00FF00'


if __name__ == "__main__":
    TSNE_algo()
    Kmean()


