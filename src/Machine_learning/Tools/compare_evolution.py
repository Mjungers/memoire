import matplotlib.pyplot as plt
import pandas as pd
from numpy import mean, std
from sklearn.metrics import plot_precision_recall_curve
from sklearn.model_selection import train_test_split, KFold, cross_validate, cross_val_score

from Machine_learning.Feature_engeniering.feature_info import *
from Machine_learning.Feature_engeniering.get_feature import get_features_id
from src.Machine_learning.Dataset.dataset import load_dataset

"""
These function can compare the various evolution of the result of an algorithm when adding new features
"""


def evolution_pre_rec_curve(Xs,Ys,clf,names):
    """
    Plot the precision recall curve for a clasifier with the various features
    :param clf: the classifier
    :return: None
    """
    precision_list = []
    reccall_list = []
    average_precison_list =[]
    for (X,y,name) in zip(Xs,Ys,names):

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)
        clf = clf.fit(X_train, y_train)
        #precision, recall, thresholds = precision_recall_curve(y_test, y_pred)
        disp = plot_precision_recall_curve(clf,X_test,y_test)
        average_precison_list.append(disp.average_precision)
        reccall_list.append(disp.recall.tolist())
        precision_list.append(disp.precision.tolist())
        plt.show()
    for(precision,recall,average_precison,name) in zip(precision_list,reccall_list,average_precison_list,names):
        plt.plot(recall,precision,label=name+" Average precision: %.3f"%(average_precison))
    plt.legend()
    plt.show()

def evolution_score(X,Y,columns,clf,scoring,feateares_list,names):
    """,
    return the evolution of the specified score of one clasifier with the addition of features
    :param clf: the scikit learn clasifier
    :param scoring: a list or dict of the score used to compare see cross_validate scoring
    :return: panda dataframe of the result
    """
    stats = {}
    total_features = []
    print("starting evolution")
    for (ids,name) in zip(feateares_list,names):
        total_features = ids + total_features
        print(len(total_features))
        X_temp = [item[total_features] for item in X]
        kfold = KFold(n_splits=10)
        scores_result = cross_validate(clf,X_temp,Y,scoring=scoring,cv=kfold)
        stats_temps={}
        for score_name in scoring:
            stats_temps[score_name] = "%.3f (%.3f)"%(mean(list(scores_result['test_'+score_name])),std(list(scores_result['test_'+score_name])))
        stats[name]=stats_temps
    return pd.DataFrame.from_dict(stats)



def box_plot(clf, scoring):
    """
    plot the box_plot of the evolution of as specific score when adding features
    :param clf:
    :param scoring:
    :return:
    """
    X, Y, columns = load_dataset()
    results = []
    features_id = get_features_id(columns)
    for (last_id, name) in zip(list(features_id), PACK_FEATURE_NAME):
        X_temp = [item[last_id:] for item in X]
        X_train, X_test, y_train, y_test = train_test_split(X_temp, Y, test_size=0.25)
        kfold = KFold(n_splits=10)
        cv_result = cross_val_score(clf, X_train, y_train, cv=kfold, scoring=scoring)
        results.append(cv_result)
    fig = plt.figure()
    fig.suptitle('Comparison between different MLAs')
    ax = fig.add_subplot(111)
    plt.boxplot(results)
    ax.set_xticklabels(PACK_FEATURE_NAME)
    plt.show()

