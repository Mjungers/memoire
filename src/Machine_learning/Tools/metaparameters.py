import pandas as pd
from sklearn.model_selection import GridSearchCV, KFold
from sklearn.preprocessing import StandardScaler

from Machine_learning.Dataset.dataset import load_dataset
from paths import PROJECT_ROOT


def optimize_parameters(clf,parameters):
    """
    select the best hyper parameter of a random forest for the scores:
    'accuracy','f1' and 'roc_auc'
    :param clf: the classifier on which to optimize the metaparameters
    :param parameters: the parameters to optimize and the
    :return: None
    """
    X, Y, columns = load_dataset(PROJECT_ROOT/"output"/"dataset"/'final_dataset.csv')
    scaler = StandardScaler().fit(X)
    X_scaled = scaler.transform(X)
    scorer =['f1']
    kfold = KFold(n_splits=10)

    result={}
    best_param={}
    for score in scorer:
        print(score)
        param_search= GridSearchCV(clf,parameters,scoring=score,cv=kfold,n_jobs=3)
        param_search.fit(X_scaled,Y)
        result[score]=param_search.best_score_

        best_param[score]=param_search.best_params_
    print(param_search)
    print(result)
    print(pd.DataFrame.from_dict(best_param).to_latex())