import pandas as pd
from numpy import mean, std
from sklearn.model_selection import KFold, cross_validate
from sklearn.preprocessing import StandardScaler

from src.Machine_learning.Dataset.dataset import load_even_dataset, load_dataset


def features_score(clf,scoring,features,even_dataset = True):
    """
    return the evolution of the specified score of one clasifier with the addition of features
    :param clf: the scikit learn clasifier
    :param scoring: a list or dict of the score used to compare see cross_validate scoring
    :return: panda dataframe of the result
    """
    if even_dataset:
        X,Y,colums= load_even_dataset()
    else:
        X, Y, colums = load_dataset()
    scaler = StandardScaler().fit(X)
    X_scaled = scaler.transform(X)
    features_id = colums.get_indexer(features)
    stats = {}
    kfold = KFold(n_splits=10)
    scores_result = cross_validate(clf, X_scaled, Y, scoring=scoring, cv=kfold, n_jobs=2)
    stats_temps = {}
    for (id_feature,name) in zip(list(features_id),features):
        X_temp = [list(item[:]).remove(id_feature) for item in X_scaled]
        kfold = KFold(n_splits=10)
        scores_result = cross_validate(clf,X_temp,Y,scoring=scoring,cv=kfold,n_jobs=2)
        stats_temps={}
        for score_name in scoring:
            stats_temps[score_name] = "%.3f (%.3f)"%(mean(list(scores_result['test_'+score_name])),std(list(scores_result['test_'+score_name])))
        stats[name]=stats_temps
    return pd.DataFrame.from_dict(stats)

def compare_score(list_X,list_Y,list_names,clf,scoring):
    """
        Compare the result of the of the classifier with the group of feature_1,feature_2 and the combinaison of the two
        :param X: the feature matrix
        :param Y: the list of class
        :param colums the name of the features
        :param clf: the scikit learn clasifier
        :param scoring: a list or dict of the score used to compare see cross_validate scoring
        :param features_1: a list of features name
        :param features_2: a list of features
        :return: panda dataframe of the result
        """

    stats = {}
    kfold = KFold(n_splits=10)
    for X, Y,name in zip(list_X,list_Y,list_names):
        print("computing:" + name)
        scores_result = cross_validate(clf, X, Y, scoring=scoring, cv=kfold, n_jobs=2)
        stats_temps = {}
        for score_name in scoring:
            stats_temps[score_name] = "%.3f (%.3f)" % (
            mean(list(scores_result['test_' + score_name])), std(list(scores_result['test_' + score_name])))
        stats[name] = stats_temps
    return pd.DataFrame.from_dict(stats)
def compare_duplicate_features_score(X,Y,colums,clf,scoring,features_1,features_2):
    """
    Compare the result of the of the classifier with the group of feature_1,feature_2 and the combinaison of the two
    :param X: the feature matrix
    :param Y: the list of class
    :param colums the name of the features
    :param clf: the scikit learn clasifier
    :param scoring: a list or dict of the score used to compare see cross_validate scoring
    :param features_1: a list of features name
    :param features_2: a list of features
    :return: panda dataframe of the result
    """
    X_scaled = StandardScaler().fit_transform(X)
    features1_id = colums.get_indexer(features_1)
    features2_id = colums.get_indexer(features_2)
    stats = {}
    kfold = KFold(n_splits=10)
    for id_features,name in zip([list(features1_id),list(features2_id),list(features1_id)+list(features2_id)],["list1","list2","both"]):
        print("computing:"+name)
        X_temp = [list(item[id_features]) for item in X_scaled]
        scores_result = cross_validate(clf,X_temp,Y,scoring=scoring,cv=kfold,n_jobs=2)
        stats_temps={}
        for score_name in scoring:
            stats_temps[score_name] = "%.3f (%.3f)"%(mean(list(scores_result['test_'+score_name])),std(list(scores_result['test_'+score_name])))
        stats[name]=stats_temps
    return pd.DataFrame.from_dict(stats)