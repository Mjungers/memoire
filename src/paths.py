from pathlib import Path

PROJECT_ROOT = Path(__file__).parents[1]

CSV_FILE=PROJECT_ROOT/"output"/"dataset"/'dataset.csv'
CSV_EVEN_FILE=PROJECT_ROOT/"output"/"dataset"/'even_dataset.csv'
CSV_TEXT_FILE=PROJECT_ROOT/"output"/"dataset"/'dataset_text_feature.csv'
CSV_TEXT_DATASET_FILE=PROJECT_ROOT/"output"/"dataset"/'dataset_text_feature_final.csv'

STAT_OUTPUT = PROJECT_ROOT/"output"/"statistical_analysis"