import psycopg2


class Database:
    """
    used to make queries on the database
    """
    port = None
    username = None
    password = None
    name = None
    connection = None


    def __init__(self,port,username,password,name):
        """
        initiate the connection to the database
        :param port: the port of the database
        :param username: the username to connect to the database
        :param password: the user password
        :param name: the name of the database
        """
        self.port=port
        self.username=username
        self.password = password
        self.name=name
        self.connection = psycopg2.connect(host="localhost",port = port,database=name,user=username,password=password)


    def __make_query(self,query,value):
        """
        execute a query on the database
        :param query:
        :param value:
        :return:
        """
        cur = self.connection.cursor()
        cur.execute(query,value)
        query_result = cur.fetchall()
        cur.close()
        return query_result

    def close_db(self):
        """
        close the connection to the database
        :return: None
        """
        self.connection.close()

    def get_first(self):
        """
        get first 10 commit of the database
        (used for testing)
        :return: query result
        """
        return self.__make_query("""SELECT id,author_name from export.commits
        LIMIT 10""")
    def get_author(self):
        """
        Return all the author of the databases
        :return:  a list of the authors
        """
        return self.__make_query("""SELECT DISTINCT author_name from export.commits""")

    def get_commits(self):
        """
        query all the commits of the database
        :return: a list with all the commits
        """
        return self.__make_query("""SELECT id,patch from export.commits""")

    def set_nbr_changed(self,id,nbr_file_changed):
        """
        set the nbr file change entry of the database for the specific commit
        :param id: id of the commit
        :param nbr_file_changed: the number of file changed
        :return:query result
        """
        return  self.__make_query("""UPDATE export.commits
        SET files_changed = %s
        WHERE id = %s"""%(str(id),str(nbr_file_changed)))

    def get_all_rep_id(self,export=True):
        """
        get all the repository id of the database
        :param export: from which database we collect the data export or not
        :return: the query result
        """
        if export:
            return self.__make_query("""SELECT id from export.repositories""",None)
        else:
            return self.__make_query("""SELECT id from public.repositories""", None)

    def get_repository(self, id,export=True):
        """
        get repository data
        :param id: the id of the repository
        :param export: from which database we collect the data export or not
        :return: the query result
        """
        cur = self.connection.cursor()
        if export:
            cur.execute("""SELECT * from export.repositories WHERE id = %s""", (id,))
        else:
            cur.execute("""SELECT * from public.repositories WHERE id = %s""", (id,))
        query_result = cur.fetchall()
        cur.close()
        return query_result

    def get_commits(self, rep_id,export=True):
        """
        get all commits for the repository
        :param rep_id: the repository ids
        :param export: from which database we collect the data export or not
        :return: the query result
        """
        cur = self.connection.cursor()
        if export:
            cur.execute("""SELECT * from export.commits WHERE repository_id = %s""",(rep_id,))
        else:
            cur.execute("""SELECT * from public.commits WHERE repository_id = %s""", (rep_id,))
        query_result = cur.fetchall()
        cur.close()
        return query_result

    def get_cve(self, cve):
        """
        get cve from with the selected id
        :param cve: the cve id
        :return: the content of the database
        """
        cur = self.connection.cursor()
        cur.execute("""SELECT * from export.cves WHERE id = %s""",(cve,))
        query_result = cur.fetchall()
        cur.close()
        return query_result

    def insert_rep(self, rep_data):
        """
        insert the repository in the database
        :param rep_data:  the repository data
        :return: None
        """
        assert(len(rep_data)== 17)
        cur = self.connection.cursor()
        cur.execute("""INSERT INTO public.repositories 
        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""", rep_data)
        cur.close()
        self.connection.commit()
        return None

    def insert_commit(self,commit_data):
        """
        insert the commit in the database
        :param commit_data:  the commit data
        :return: None
        """
        assert (len(commit_data) == 26)
        cur = self.connection.cursor()
        cur.execute("""INSERT INTO public.commits 
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""", commit_data)
        cur.close()
        self.connection.commit()
        return None

    def insert_cve(self,cve_data):
        """
        insert the cve data into the cves table of the database
        :param cve_data: the complete data of the row
        :return: None
        """
        assert (len(cve_data) == 14)
        cur = self.connection.cursor()
        cur.execute("""INSERT INTO public.cves 
                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""", cve_data)
        cur.close()
        self.connection.commit()
        return None

    def get_commits_ordered(self,rep_id,export=False):
        """
        request all the commits from one repository ordered by the time commit was made
        :param rep_id: the repository id in the database
        :param export: select the type of database export or public
        :return: the result of the query
        :rtype: tuple
        """
        cur = self.connection.cursor()
        if export:
            cur.execute("""SELECT * from export.commits WHERE repository_id = %s ORDER BY committer_when ASC""",
                        (rep_id,))
        else:
            cur.execute("""SELECT * from public.commits WHERE repository_id = %s ORDER BY committer_when ASC""", (rep_id,))
        query_result = cur.fetchall()
        cur.close()
        return query_result







