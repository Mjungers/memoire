import random

from src.database.database_info import PORT, PASSWORD, USERNAME, DATABASE_NAME, NEW_DB
from src.database.load_db import Database


def get_random_id(number):
    """
    return a list of random selected repository id
    :param number: number of id to return
    :return: a list of id
    """
    db = Database(PORT, USERNAME, PASSWORD, DATABASE_NAME)
    result = db.get_all_rep_id()
    list_id =[]
    while len(list_id) < number:
        id = random.randint(0,len(result))
        b = result[id]
        result.remove(b)
        repository = db.get_repository(b[0])
        if(repository[0][16]) != 0:
            list_id.append(b[0])
    db.close_db()
    return list_id

def set_sub_db(list_id):
    """
    fill the new database with the  data of the selected repository
    :param list_id: the list of the repository id
    :return: None
    """
    db = Database(PORT, USERNAME, PASSWORD, DATABASE_NAME)
    db2 = Database(PORT, USERNAME, PASSWORD, NEW_DB)
    cves = []
    for id in list_id:
        commits = db.get_commits(id)
        for commit in commits:
            db2.insert_commit(commit)
            if commit[23] != '':
                if not(commit[23] in cves):
                    cves.append(commit[23])
                    cve = db.get_cve(commit[23])
                    if len(cve) == 1:
                        db2.insert_cve(cve[0])
    db.close_db()




def update_sub_db():
    """
    Update the database with the commits
    :return: None
    """
    new_db = Database(PORT, USERNAME, PASSWORD, NEW_DB)
    commits = new_db.get_all_rep_id(False)
    set_sub_db(commits)
    print("finished")



