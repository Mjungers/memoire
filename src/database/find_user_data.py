import requests
from pyarrow import json

from src.database.database_info import PORT, PASSWORD, USERNAME, DATABASE_NAME
from src.database.load_db import Database

"""
This file was used to find data about the authors on the git website but it was not used in the final project
"""

def get_author_id(start_point,user,found,users_data,missing):
    """
    get get author data on github
    :param start_point: the number of author already computed
    :param user: number of user
    :param found: number of user found
    :param users_data: the list of all users data
    :param missing: the missing user data
    :return: None
    """
    nbr_user=user
    nbr_found=found
    new_start_point = start_point
    print("connecting to db")
    db = Database(PORT, USERNAME, PASSWORD, DATABASE_NAME)
    print("making query")
    result = db.get_author()
    db.close_db()
    for tmp in result[start_point:]:
        found=False
        nbr_user+=1

        user = tmp[0]
        user.replace(" ", "%20")
        json_search = requests.get("https://api.github.com/search/users?q="+user).text
        result = json.loads(json_search)
        if('message'in result):
            return {"nbr_user":nbr_user-1,"nbr_found":nbr_found,"start_point": new_start_point,"users_data":users_data,"not_found":missing}
        for test_user in result["items"]:
            url = test_user["url"]
            rcv_user = requests.get(url).text
            json_user = json.loads(rcv_user)
            if('message' in json_user):

                return {"nbr_user":nbr_user-1,"nbr_found":nbr_found,"start_point": new_start_point,"users_data":users_data,"not_found":missing}
            elif(user.replace("%20"," ").lower()==json_user["name"].lower()):
                nbr_found+=1
                found=True
                users_data.append(json_user)
        if(not(found)):
            missing.append(user.replace("%20"," "))
        new_start_point += 1
    print("end")
    print(nbr_user)
    print(nbr_found)
    print(nbr_found/nbr_user)


def get_all_json():
    """
    update the content of the json
    :return:
    """
    with open('users.txt') as json_file:
        data = json.load(json_file)
    new_data = get_author_id(data["start_point"], data["nbr_user"], data["nbr_found"], data["users_data"], data["not_found"])
    with open('users.txt','w') as outfile:
        json.dump(new_data,outfile)

def get_data_json():
    """
    retriev data from the request result
    :return:
    """
    with open('users.txt') as json_file:
        data = json.load(json_file)
        print("%s vs %s users have been searched, with %s vs %s found"%(str(data["nbr_user"]), str(len(data["users_data"])+len(data["not_found"])),str(data["nbr_found"]),str(len(data["users_data"]))))
        users= []
        for user in data["users_data"]:
            if(not(user["name"] in users)):
                users.append(user["name"])
        print(users)
        print("distinct user %s"%(str(len(users))))

def init():
    """
    initiate the json file to load the users data
    :return:
    """
    new_data={"nbr_user":0,"nbr_found":0,"start_point": 0,"users_data":[],"not_found":[]}
    with open('users.txt','w') as outfile:
        json.dump(new_data,outfile)