import unittest

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import euclidean_distances
from unidiff import PatchSet

from Machine_learning.Feature_engeniering.commit import get_moddification, true_splitline
from src.Machine_learning.Feature_engeniering.commit import get_complexity_features, remove_comment
from src.database.database_info import PORT, PASSWORD, USERNAME, DATABASE_NAME
from src.database.load_db import Database
from src.database.mange_db import get_random_id


class MyTestCase(unittest.TestCase):
    def test_db(self):
        print("connecting to db")
        db = Database(PORT, USERNAME, PASSWORD, DATABASE_NAME)
        print("making query")
        result = db.get_first(10)
        print(result)
        db.close_db()
        #get a more precise test
        self.assertIsInstance(result,list)

    def test_get_change(self):
        commit = ("""diff --git a/src/modules/m_spanningtree.cpp b/src/modules/m_spanningtree.cpp
index 03a03cf..5263f3f 100644
--- a/src/modules/m_spanningtree.cpp
+++ b/src/modules/m_spanningtree.cpp
@@ -856,8 +856,8 @@ class TreeSocket : public InspSocket
 		while (!s.eof())
 		{
 			s >> param;
-			if ((param != "") && (param != "\n"))
-			{
+			//if ((param != "") && (param != "\n"))
+			//{
 				if ((param.c_str()[0] == ':') && (item))
 				{
 					char* str = (char*)param.c_str();
@@ -876,7 +876,7 @@ class TreeSocket : public InspSocket
 				}
 				item++;
 				n.push_back(param);
-			}
+			//}
 		}
 		return n;
 	}
""",)
        test_modification = [(["""			if ((param != "") && (param != "\n"))""","""			{"""],["""			//if ((param != "") && (param != "\n"))""","""			//{"""])]
        addition = 0  # 390
        delletion = 0  # 386
        test = get_moddification(commit[0])

        for modification in test['modification']:
            addition += len(modification[0])
            delletion += len(modification[1])
        addition += len(test["addition"])
        delletion += len(test["delletion"])


        print(test["next"])
        print("ok\n \n")
        print(test["previous"])
        self.assertEqual(test["header"],[],"header not ok")
        self.assertEqual(test["nbrfunction"],2)
        self.assertEqual(test["nbrfile"], 1)
        self.assertEqual(3, addition)
        self.assertEqual(3, delletion)

    def test_get_change2(self):
        commit = ("""diff --git a/src/lxc/lxc_destroy.c b/src/lxc/lxc_destroy.c
index 686b303..9a1b11f 100644
--- a/src/lxc/lxc_destroy.c
+++ b/src/lxc/lxc_destroy.c
@@ -50,7 +50,7 @@ static struct lxc_arguments my_args = {
 	.help     = "\
 --name=NAME [-f] [-P lxcpath]\n\
 \n\
-lxc-stop stops a container with the identifier NAME\n\
+lxc-destroy destroys a container with the identifier NAME\n\
 \n\
 Options :\n\
   -n, --name=NAME   NAME for name of the container\n\
    """,)
        addition = 0  # 390
        delletion = 0  # 386
        test = get_moddification(commit[0])

        for modification in test['modification']:
            addition += len(modification[0])
            delletion += len(modification[1])
        addition += len(test["addition"])
        delletion += len(test["delletion"])

        print(test["next"])
        print("ok\n \n")
        print(test["previous"])
        self.assertEqual(test["header"], [], "header not ok")
        self.assertEqual(test["nbrfunction"], 1)
        self.assertEqual(test["nbrfile"], 1)
        self.assertEqual(1, addition)
        self.assertEqual(1, delletion)
    def test_remove_comment(self):
        commit = """
 		while (!s.eof())
 		{
 			s >> param;
+			//if ((param != "") && (param != "\n"))
+			//{
 				if ((param.c_str()[0] == ':') && (item))
 				{
 					char* str = (char*)param.c_str();
@@ -876,7 +876,7 @@ class TreeSocket : public InspSocket
 				}
 				item++;
 				n.push_back(param);
+			//}
 		}
 		return n;
 	}
"""
        commit_result = """
 		while (!s.eof())
 		{
 			s >> param;
+			
+			
 				if ((param.c_str()[0] == ':') && (item))
 				{
 					char* str = (char*)param.c_str();
@@ -876,7 +876,7 @@ class TreeSocket : public InspSocket
 				}
 				item++;
 				n.push_back(param);
+			
 		}
 		return n;
 	}
"""
        for line_r,line_t in zip(true_splitline(remove_comment(commit)),true_splitline(commit_result)):
            assert line_r == line_t


    def test_complexity(self):
        commit =["""
        while (!s.eof())
        {
            s >> param;
-			if ((param != "") && (param != "\n"))
-			{
                if ((param.c_str()[0] == ':') && (item))
                {
                    char* str = (char*)param.c_str();
@@ -876,7 +876,7 @@ class TreeSocket : public InspSocket
                }
                item++;
                n.push_back(param());
-			}
        }
        return test(x,y);
    }""",
       """while (!s.eof())
        {
            s >> param;
                 + // if ((param != "") & & (param != "\n"))
                         + // {
        if ((param.c_str()[0] == ':') & & (item))
        {
        char * str = (char * )param.c_str();

        class TreeSocket:
            public

        InspSocket

    }
    item + +;
    n.push_back(param);
    +			//}
    }
    return test( x,y);
    }"""]
        test = get_complexity_features(commit)
        assert test["Comp_functcall_bef"] ==6
        print(test)
    def test_vector(self):
        commit = ["""diff --git a/src/modules/m_spanningtree.cpp b/src/modules/m_spanningtree.cpp
        index 03a03cf..5263f3f 100644
        --- a/src/modules/m_spanningtree.cpp
        +++ b/src/modules/m_spanningtree.cpp
        @@ -856,8 +856,8 @@ class TreeSocket : public InspSocket
         		while (!s.eof())
         		{
         			s >> param;
        -			if ((param != "") && (param != "\n"))
        -			{
        +			//if ((param != "") && (param != "\n"))
        +			//{
         				if ((param.c_str()[0] == ':') && (item))
         				{
         					char* str = (char*)param.c_str();
        @@ -876,7 +876,7 @@ class TreeSocket : public InspSocket
         				}
         				item++;
         				n.push_back(param);
        -			}
        +			//}
         		}
         		return n;
         	}
        ""","""diff --git a/src/modules/m_spanningtree.cpp b/src/modules/m_spanningtree.cpp
        index 03a03cf..5263f3f 100644
        --- a/src/modules/m_spanningtree.cpp
        +++ b/src/modules/m_spanningtree.cpp
        @@ -856,8 +856,8 @@ class TreeSocket : public InspSocket
         		while (!s.eof())
         		{
         			s >> param;
        -			if ((param != "") && (param != "\n"))
        -			{
        +			//Then ((param != "") && (param != "\n"))
        +			//{
         				Import ((param.c_str()[0] == ':') && (item))
         				{
         					char* int = (char*)param.c_str();
        @@ -876,7 +876,7 @@ class TreeSocket : public InspSocket
         				}
         				item++;
         				n.push_back(param);
        -			}
        if then i become the best cat
        +			//}
         		}
         		return n;
         	}"""]
        test2="""diff --git a/src/modules/m_spanningtree.cpp b/src/modules/m_spanningtree.cpp
        index 03a03cf..5263f3f 100644
        --- a/src/modules/m_spanningtree.cpp
        +++ b/src/modules/m_spanningtree.cpp
        @@ -856,8 +856,8 @@ class TreeSocket : public InspSocket
         		while (!s.eof()) i love konfu
         		{
         			s >> param;
        -			if ((param != "") && (param != "\n"))
        -			{
         				Import ((param.c_str()[0] == ':') && (item))
         				{
         					char* int = (char*)param.c_str();
        @@ -876,7 +876,7 @@ class TreeSocket : public InspSocket
         				}
         				item++;
         				n.push_back(param);
        -			}
        if then i become the best cat in the wolrd
         		}
         		return n;
         	}
         	index 686b303..9a1b11f 100644
            --- a/src/lxc/lxc_destroy.c
            +++ b/src/lxc/lxc_destroy.c
            @@ -50,7 +50,7 @@ static struct lxc_arguments my_args = {
                .help     = "\
             --name=NAME [-f] [-P lxcpath]\n\
             \n\
            -lxc-stop stops a container with the identifier NAME\n\
            +lxc-destroy destroys a container with the identifier NAME\n\
             \n\
             Options :\n\
               -n, --name=NAME   NAME for name of the container\n\ """
        corpus = commit
        vectorizer = CountVectorizer()
        features = vectorizer.fit_transform(corpus).todense()
        print(vectorizer.vocabulary_)
        for f in features:
            print(f)
            print(vectorizer.get_feature_names())

            print(euclidean_distances(features[0], f))
        commit[1]=test2
        corpus = commit
        vectorizer2 = CountVectorizer()
        features2 = vectorizer2.fit_transform(corpus).todense()
        print(vectorizer2.vocabulary_)
        for f in features2:
            print(euclidean_distances(features2[0], f))

    def test_get_change_mf(self):
        commit_file = open("testfile/commit_long.txt","r")
        nbr_addition = 0#390
        nbr_delletion = 0#386
        commit = [(commit_file.read(),)]
        print(commit)
        test = get_moddification(commit)[0]
        for modification in test['modification']:
            nbr_addition += len(modification[1])
            nbr_delletion += len(modification[0])
        for addition in test["addition"]:
            nbr_addition += len(addition)
        for delletion in test["delletion"]:
            nbr_delletion += len(delletion)

        self.assertEqual(390,nbr_addition)
        self.assertEqual(386,nbr_delletion)
        self.assertEqual(4,test["nbrfile"])

    def test_truesplitline(self):
        list = true_splitline("""salut je suis \"\\n" t \n et toi?\n matthieu:"je suis \\n et toi?\\n"\nnouveau""")
        self.assertEqual(list[0],"""salut je suis "\\n" t""")
        self.assertEqual(list[2],""" matthieu:"je suis \\n et toi?\\n\"""")

    def test_id(self):
        list = get_random_id(10)
        self.assertEqual(len(list),10)
        print(list)

    def test_get_sub(self):
        get_sub_db(3)

    def test_insert(self):
        PORT = 5432
        USERNAME = "postgres"
        DATABASE_NAME = "vcc-db"
        PASSWORD = "admin"
        print("connecting to db")
        db1 = Database(PORT, USERNAME, PASSWORD, DATABASE_NAME)

        DATABASE_NAME = "test-db"
        print("connecting to db")
        db2 = Database(PORT, USERNAME, PASSWORD, DATABASE_NAME)

        list_id = get_random_id(10)
        for id in list_id:
            repository = db1.get_repository(id)[0]
            print(repository)
            db2.insert_rep(repository)
        db1.close_db()
        db2.close_db()
    def test_dif(self):
        test=""""index 686b303..9a1b11f 100644
--- a/src/lxc/lxc_destroy.c
+++ b/src/lxc/lxc_destroy.c
@@ -50,7 +50,7 @@ static struct lxc_arguments my_args = {
 	.help     = "\
 --name=NAME [-f] [-P lxcpath]\n\
 \n\
-lxc-stop stops a container with the identifier NAME\n\
+lxc-destroy destroys a container with the identifier NAME\n\
 \n\
 Options :\n\
   -n, --name=NAME   NAME for name of the container\n\
            """
        patch = PatchSet(test)
        hunk = patch[0][0]
        added =str(hunk.target)
        removed=str(hunk.source)
        print(added)
        print(removed)


if __name__ == '__main__':
    unittest.main()

